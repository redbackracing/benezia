from distutils.core import setup

with open('requirements.txt', 'r') as f:
    requirements = [line for line in f.readlines()]

setup(
    name='Benezia',
    version='0.1',
    description='Server for Redback Racing datalogger',
    author='Redback Racing',
    packages=['benezia'],
    scripts=['benezia/benezia_server.py'],
    install_requires=requirements)
