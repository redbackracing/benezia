import json
import pytest
import copy
import sys
import uuid
import asyncio
import os
import datetime
from os.path import dirname, abspath, join

sys.path.append(join(dirname(dirname(abspath(__file__))), 'benezia'))

from benezia_server import app as server_app
import auth

db_filename = '{0}.db'.format(str(uuid.uuid4()))
temp_db = 'sqlite:///' + db_filename

# Valid map data for testing
map_data = [
    {'x': 100.0, 'y': 200.0},
    {'x': -100.0, 'y': -200.0},
    {'x': 150.0, 'y': 204.0},
    {'x': 180.0, 'y': 205.0},
    {'x': 100000.0, 'y': 206.0},
    {'x': 0.0, 'y': 207.0}
]

# Valid channel data for testing
channel_data = {
    'channelnum': 5,
    'channelname': 'Speed',
    'value': 1000.0,
    'time': 0.5,
    'diag': True,
    'warn': False
}

# Valid configuration data for testing
configuration_file = {'configFile': {'inputs': [
        {'channelName': 'Speed',
            'channelNumber': 5,
            'samplingRate': 10,
            'readOrder': 3,
            'valueRange': [0.1, 1.0],
            'signalRange': [0.1, 1.0],
            'conversion': [
                {'x': 6.0, 'y': 9.0},
                {'x': 4.0, 'y': 5.0}
            ]}
    ],
    'outputs': {}
    }
}

# Invalid configuration data for testing
invalid_configuration_file = {
    'configFile': {'inputs': [
        {'channelName': 'Speed',
            'channelNumber': 5.0,
            'samplingRate': 10,
            'readOrder': 3,
            'valueRange': [0.1, 1.0],
            'signalRange': [0.1, 1.0],
            'conversion': [
                {'x': 6.0, 'y': 9.0},
                {'x': 4.0, 'y': 5.0}
            ]}
    ],
    'outputs': {}
    }
}

complete_test_data = [
   {'start': '2017-08-12-1134'},
   {'config': {
       'field': 6,
       'param': 'a'
   }},
   {'channelname': 'channel 2',
    'channelnum': 2,
    'diag': True,
    'time': 0,
    'value': 63.875,
    'warn': True},
   {'channelname': 'first channel',
    'channelnum': 1,
    'diag': True,
    'time': 0.5,
    'value': 69.808,
    'warn': True},
   {'channelname': 'Test Temperature Sensor',
    'channelnum': 7,
    'diag': True,
    'time': 1,
    'value': 72.599,
    'warn': True}
]

dashboard_file = {
    'charts': [{
        'top': 5,
        'left': 4,
        'height': 3,
        'width': 2,
        'class': 'area',
        'dataset': 1,
        'channel': 1
    },
    {
        'top': 5,
        'left': 4,
        'height': 3,
        'width': 2,
        'class': 'bar',
        'dataset': 2,
        'channel': 2
    }]
}


class TempDatabase():

    def __init__(self):
        self.filename = '{0}.db'.format(str(uuid.uuid4()))
        self.path = 'sqlite:///' + self.filename

    def __enter__(self):
        return self.path

    def __exit__(self, *args):
        os.remove(self.filename)


# Fixture code from yunstanford at https://github.com/yunstanford/pytest-sanic/issues/1
@pytest.yield_fixture
def app():
    yield server_app


@pytest.fixture
def test_app(loop, app, test_client):
    return loop.run_until_complete(test_client(app))


async def test_add_user(test_app):
    username = 'test_user'
    password = 'hunter2'

    with TempDatabase() as db:
        data = {
            'username': username,
            'password': password,
            'database': db
        }

        # Add the temporary user
        response = await test_app.post('/users', data=json.dumps(data))

        assert response.status == 200
        assert auth.verify_user(username, password, db)


# Test the saving and loading of valid maps
async def test_save_and_load_map(test_app):

    with TempDatabase() as db:
        # Define the POST body
        data = {'map': map_data,
                'name': 'Test map',
                'database': db}

        # Save data in temporary database
        response = await test_app.post('/maps', data=json.dumps(data))

        # Ensure that saving was successful
        assert response.status == 200
        assert await response.text() == 'OK'

        # Load the saved data
        params = {'name': 'Test map',
                  'database': db}

        response = await test_app.get('/maps', params=params)

        valid_response = {
            'map': map_data,
        }

        assert await response.json() == valid_response

# Test the saving and loading of valid dashboard configuration
async def test_save_and_load_dashboard(test_app):
    username = 'test_user'
    password = 'hunter2'

    with TempDatabase() as db:
        # Add the temporary user
        user_data = {
            'username': username,
            'password': password,
            'database': db
        }

        response = await test_app.post('/users', data=json.dumps(user_data))

        assert response.status == 200

        # Define the POST body
        data = {'dashboard': dashboard_file,
                'database': db,
                'username': username,
                'password': password}

        # Save data in temporary database
        response = await test_app.post('/dashboard', data=json.dumps(data))

        # Ensure that saving was successful
        assert response.status == 200
        assert await response.text() == 'OK'

        # Load the saved data
        params = {'username': username,
                  'password': password,
                  'database': db}

        response = await test_app.get('/dashboard', params=params)

        valid_response = {
            'dashboard': dashboard_file,
        }

        assert await response.json() == valid_response


# Test that saving a second dashboard configuration for the same user overwrites the first
async def test_overwrite_dashboard(test_app):
    username = 'test_user'
    password = 'hunter2'

    with TempDatabase() as db:
        # Add the temporary user
        user_data = {
            'username': username,
            'password': password,
            'database': db
        }

        response = await test_app.post('/users', data=json.dumps(user_data))

        assert response.status == 200

        # Define the POST body
        data = {'dashboard': dashboard_file,
                'database': db,
                'username': username,
                'password': password}

        # Save data in temporary database
        response = await test_app.post('/dashboard', data=json.dumps(data))

        # Ensure that saving was successful
        assert response.status == 200
        assert await response.text() == 'OK'

        # Define the POST body for the second dashboard
        new_dashboard = dashboard_file
        new_dashboard['charts'][0]['top'] = 20

        data = {'dashboard': new_dashboard,
                'database': db,
                'username': username,
                'password': password}

        # Save new dashboard in temporary database
        response = await test_app.post('/dashboard', data=json.dumps(data))

        # Ensure that saving was successful
        assert response.status == 200
        assert await response.text() == 'OK'

        # Load the saved data
        params = {'username': username,
                  'password': password,
                  'database': db}

        response = await test_app.get('/dashboard', params=params)

        # Make sure that the new dashboard has overwritten the old one
        valid_response = {
            'dashboard': new_dashboard,
        }

        assert await response.json() == valid_response


# Test the saving and loading of valid channel and configuration data, as well as loading individal
# channel data.
async def test_valid_save_and_load(test_app):

    username = 'test_user'
    password = 'hunter2'

    with TempDatabase() as db:
        # Add the temporary user
        user_data = {
            'username': username,
            'password': password,
            'database': db
        }

        response = await test_app.post('/users', data=json.dumps(user_data))

        assert response.status == 200

        # Define the POST body
        data = {'data': complete_test_data,
                'database': db,
                'username': username,
                'password': password}

        # Save data in temporary database
        response = await test_app.post('/data', data=json.dumps(data))

        # Ensure that saving was successful
        assert response.status == 200
        assert await response.text() == 'OK'

        # Get the date to retrieve the data
        date = complete_test_data[0]['start']

        # Load the saved data
        params = {'username': username,
                  'password': password,
                  'database': db,
                  'test': date}

        response = await test_app.get('/data', params=params)

        valid_response = {
            'data': complete_test_data
        }

        assert await response.json() == valid_response

        # Load a specific data channel
        params = {'username': username,
                  'password': password,
                  'database': db,
                  'test': date,
                  'channel': complete_test_data[2]['channelname']}

        response = await test_app.get('/data', params=params)

        valid_response = {
            'data': complete_test_data[2],
            'config': complete_test_data[1]
        }

        assert await response.json() == valid_response


# Test that empty save requests trigger an error
async def test_save_data_invalid_request(test_app):
    response = await test_app.post('/data')

    assert response.status == 400
    assert await response.text() == 'Invalid request'


# Test that attempting to load data without a correct user fails
async def test_get_data_incorrect_user(test_app):

    with TempDatabase() as db:
        # Define the request
        params = {'username': 'dummy',
                  'password': 'dummy',
                  'database': db,
                  'test': '2017-08-12-1134'}

        response = await test_app.get('/data', params=params)

        assert response.status == 403
        assert await response.text() == 'User does not exist'


# Test that attempting to load data without a correct password fails
async def test_get_data_incorrect_password(test_app):
    username = 'test_user'
    password = 'hunter2'

    with TempDatabase() as db:
        data = {
            'username': username,
            'password': password,
            'database': db
        }

        # Add the temporary user
        response = await test_app.post('/users', data=json.dumps(data))

        assert response.status == 200

        # Define the request
        params = {'username': username,
                  'password': 'dummy',
                  'database': db,
                  'test': '2017-08-12-1134'}

        response = await test_app.get('/data', params=params)

        assert response.status == 403
        assert await response.text() == 'Invalid password'


# Test handling of multiple data files
async def test_multiple_data_files(test_app):
    username = 'test_user'
    password = 'hunter2'

    with TempDatabase() as db:
        # Add the temporary user
        user_data = {
            'username': username,
            'password': password,
            'database': db
        }

        response = await test_app.post('/users', data=json.dumps(user_data))

        assert response.status == 200

        # Define and save the first test
        test_one_data = {'data': complete_test_data,
                         'database': db,
                         'username': username,
                         'password': password}

        response = await test_app.post('/data', data=json.dumps(test_one_data))

        assert response.status == 200
        assert await response.text() == 'OK'

        # Define and save the second test. Test data must be deep copied to avoid mutating
        # the original.
        second_test_data = copy.deepcopy(complete_test_data)
        second_test_data[0]['start'] = '2017-08-13-1134'

        test_two_data = {'data': second_test_data,
                         'database': db,
                         'username': username,
                         'password': password}

        response = await test_app.post('/data', data=json.dumps(test_two_data))

        assert response.status == 200
        assert await response.text() == 'OK'

        # Get the date of the first test to retrieve the data
        date = complete_test_data[0]['start']

        # Load and check test one
        params = {'username': username,
                  'password': password,
                  'database': db,
                  'test': date}

        response = await test_app.get('/data', params=params)

        valid_response = {
            'data': complete_test_data
        }

        assert await response.json() == valid_response

        # Get the date of the second test to retrieve the data
        date = second_test_data[0]['start']

        # Load and check test two
        params = {'username': username,
                  'password': password,
                  'database': db,
                  'test': date}

        response = await test_app.get('/data', params=params)

        valid_response = {
            'data': second_test_data
        }

        assert await response.json() == valid_response

        # Test that user info displays multiple saved data files
        params = {'username': username,
                  'password': password,
                  'database': db}

        response = await test_app.get('/info', params=params)

        valid_response = {
            'tests': ['2017-08-12-1134', '2017-08-13-1134'],
            'dashboard': 0
        }

        assert await response.json() == valid_response


# Test that the info endpoint gives correct information about the user's dashboard
async def test_dashboard_info(test_app):
    username = 'test_user'
    password = 'hunter2'

    with TempDatabase() as db:
        # Add the temporary user
        user_data = {
            'username': username,
            'password': password,
            'database': db
        }

        response = await test_app.post('/users', data=json.dumps(user_data))

        assert response.status == 200

        # Get user info
        params = {'username': username,
                  'password': password,
                  'database': db}

        response = await test_app.get('/info', params=params)

        # The user does not currently have a dashboard set
        response_json = await response.json()
        assert response_json['dashboard'] == 0

        # Define the POST body for dashboard saving
        data = {'dashboard': dashboard_file,
                'database': db,
                'username': username,
                'password': password}

        # Save data in temporary database
        response = await test_app.post('/dashboard', data=json.dumps(data))

        # Ensure that saving was successful
        assert response.status == 200
        assert await response.text() == 'OK'

        # Get user info
        params = {'username': username,
                  'password': password,
                  'database': db}

        response = await test_app.get('/info', params=params)

        # The user currently has a dashboard set
        response_json = await response.json()
        assert response_json['dashboard'] == 1
