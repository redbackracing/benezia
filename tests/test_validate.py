import sys
from os.path import dirname, abspath, join

sys.path.append(join(dirname(dirname(abspath(__file__))), 'benezia'))

from validator import validate

test_schema = {
    'configFile': {
        'inputs': [{
            'channelName':
            str,
            'channelNumber':
            int,
            'samplingRate':
            int,
            'readOrder':
            int,
            'valueRange': [float, float],
            'signalRange': [float, float],
            'conversion': [{
                'x': float,
                'y': float
            }, {
                'x': float,
                'y': float
            }],
        }],
        'outputs': {}
    }
}

test_valid_data = {
    'configFile': {
        'inputs': [{
            'channelName':
            'A channel',
            'channelNumber':
            5,
            'samplingRate':
            70,
            'readOrder':
            3,
            'valueRange': [0.0, 0.001],
            'signalRange': [5.0, 10.0],
            'conversion': [{
                'x': 10.0,
                'y': 10.0
            }, {
                'x': 9.0,
                'y': 9.0
            }],
        }],
        'outputs': {}
    }
}


def test_valid():
    assert validate(test_valid_data, test_schema)


def test_atomic():
    assert validate(5, int)
    assert validate(6.0, float)
    assert validate('string', str)
    assert validate(True, bool)

    assert validate(0, int)
    assert validate(0, float)

    assert not validate(5, str)


def test_variable_length_list():
    schema = [int]

    # An empty list is valid as it is just length 0
    assert validate([], schema)

    # Test basic usage
    assert validate([1], schema)
    assert validate([1, 2, 3], schema)

    # Test invalid usage
    assert not validate(False, schema)
    assert not validate([1, 2, 3, False], schema)


def test_fixed_length_list():
    schema = [int, float]

    # An empty list is invalid as it is not of the correct length
    assert not validate([], schema)

    # Test invalid lengths
    assert not validate([1, 2, 3, 4], schema)
    assert not validate([1], schema)

    # Test invalid contents
    assert not validate([False, False], schema)
    assert not validate([1, False], schema)
    assert not validate([False, 2.0], schema)

    # Test valid usage
    assert validate([1, 2.0], schema)


def test_dictionary():
    schema = {'a': int,
              'b': float}

    # Test invalid keys
    assert not validate({}, schema)
    assert not validate({'a': 1}, schema)
    assert not validate({'a': 1, 'b': 2, 'c': 3}, schema)

    # Test invalid values
    assert not validate({'a': False, 'b': False}, schema)
    assert not validate({'a': 1, 'b': False}, schema)

    # Test valid values
    assert validate({'a': 1, 'b': 2.0}, schema)