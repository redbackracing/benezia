import sys
from os.path import dirname, abspath, join
import os
import pytest

sys.path.append(join(dirname(dirname(abspath(__file__))), 'benezia'))

import auth
import uuid

db_filename = '{0}.db'.format(str(uuid.uuid4()))
temp_db = 'sqlite:///' + db_filename


def test_save_and_verify():
    username = 'testUser'
    password = 'hunter2'

    assert auth.store_user(username, password, temp_db)

    assert auth.verify_user(username, password, temp_db)

    # Remove temporary database
    os.remove(db_filename)


def test_invalid_password():
    username = 'testUser'
    password = 'hunter2'

    assert auth.store_user(username, password, temp_db)

    with pytest.raises(auth.AuthException) as e:
        auth.verify_user(username, 'hunter3', temp_db)

        assert str(e) == 'Invalid password'

    # Remove temporary database
    os.remove(db_filename)


def test_adding_duplicate():
    username = 'testUser'
    password = 'hunter2'

    assert auth.store_user(username, password, temp_db)

    with pytest.raises(auth.AuthException) as e:
        auth.store_user(username, password, temp_db)

        assert str(e) == 'User already exists'

    # Remove temporary database
    os.remove(db_filename)
