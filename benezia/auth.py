from passlib.hash import bcrypt
import dataset


class AuthException(Exception):
    pass


# Store a new user in the users database. Returns True if the operation was successful, an
# exception otherwise.
def store_user(username, password, database):
    # Open database
    db = dataset.connect(database)
    table = db['users']

    # Ensure the user doesn't already exist
    user = table.find_one(username=username)

    if user is not None:
        raise AuthException('User already exists')

    # Hash the password for security
    password_hash = bcrypt.hash(password)

    # Attempt to store the new data
    try:
        table.insert({'username': username, 'password': password_hash})
    except:
        raise AuthException('Could not add user to database')

    return True


# Verify a user's password. Returns True if the password is valid, an exception otherwise.
def verify_user(username, password, database):
    # Open database
    db = dataset.connect(database)
    table = db['users']

    # Find the user in the database
    user = table.find_one(username=username)

    # Check that user exists
    if user is None:
        raise AuthException('User does not exist')

    # Verify password
    if not bcrypt.verify(password, user['password']):
        raise AuthException('Invalid password')

    return True