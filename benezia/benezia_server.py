#!/usr/bin/py3.5

from sanic import Sanic
from sanic.log import log
import sanic.response as response
import socket
import snappy
import json
import datetime
import argparse
import dataset

from benezia.validator import validate, data_schema, new_user_schema, dashboard_schema
import benezia.auth as auth

default_database_path = 'db.json'

# Endpoints requiring authentification
auth_endpoints = ['/data', '/dashboard', '/info']

# Create Sanic webserver
app = Sanic()

# Define and create socket for communicating with Liara server
webserver_sock = ()
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
sock.settimeout(5.0)


@app.middleware('response')
async def add_CORS_header(request, response):
    response.headers['Access-Control-Allow-Origin'] = '*'


@app.middleware('request')
async def check_valid_request(request):
    if request.path != '/':
        # Handle both GET and POST requests
        if request.query_string != '':
            args = request.args
        else:
            args = request.json

        if args is None:
            return response.text('Invalid request', 400)


@app.middleware('request')
async def authenticate(request):
    # If the endpoint requires authentification...
    if request.path in auth_endpoints:

        # Handle both GET and POST requests
        if request.query_string != '':
            args = request.args
        else:
            args = request.json

        username = args.get('username')
        password = args.get('password')
        database = args.get('database', default_database_path)

        log.info('Authenticating user {0}...'.format(username))

        if username is None:
            log.warning('Authentification failed for user {0}: missing username'.format(username))
            return response.text('Username required', status=403)

        if password is None:
            log.warning('Authentification failed for user {0}: missing password'.format(username))
            return response.text('Password required', status=403)

        try:
            auth.verify_user(username, password, database)
        except auth.AuthException as e:
            log.warning('Authentification failed for user {0}: {1}'.format(username, str(e)))
            return response.text(str(e), status=403)

        # Delete the plaintext password so it isn't stored
        del args['password']

        log.info('Successfully authenticated user {0}'.format(username))


# Respond to GET requests on the /info endpoint with user information
@app.get('/info')
async def user_info(request):
    # Get the username that identifies the target user
    try:
        username = request.args.get('username')
    except:
        return response.text('Missing username', status=400)

    # Allow the user to specify their own database filename and connect to database
    database = request.args.get('database', default_database_path)
    db = dataset.connect(database)

    # Determine if the user has a dashboard
    dashboard_table = db['dashboards']
    saved_dashboard = dashboard_table.find_one(username=username)

    if saved_dashboard is None:
        has_dashboard = 0
    else:
        has_dashboard = 1

    # Find all user tests, placing them into a list.
    tests_table = db['data']
    tests = []

    for test in tests_table.find(username=username):
        tests.append(test['time'])

    user_info = {'tests': tests, 'dashboard': has_dashboard}

    return response.json(user_info)


# Listen for POST requests on the /maps endpoint and store the given data in a database.
@app.post('/maps')
async def save_map(request):
    # Ensure that the data is valid
    if not validate(request.json, map_schema):
        return response.text('Data is invalid', status=400)

    map_name = request.json.get('name')

    table = get_table_for_request(request, 'maps')

    # If the user has a saved dashboard already, find it in the database
    saved_map = table.find_one(name=map_name)

    data = {'name': map_name, 'map': json.dumps(request.json['map'])}

    # Attempt to store the new data
    try:
        if saved_map is None:
            table.insert(data)
        else:
            table.update(data, ['name'])
    except Exception as e:
        return response.text('Error saving data in database', status=500)

    # Notify the user that the map was stored successfully
    return response.text('OK')


# Listen for GET requests on the /maps endpoint and return the specified map
@app.get('/maps')
async def get_map(request):

    # Get the name that identifies the target map
    try:
        map_name = request.args.get('name')
    except:
        return response.text('Missing name of map', status=400)

    table = get_table_for_request(request, 'maps')

    # Search the database for the correct data
    data = table.find_one(name=map_name)

    # Ensure that the data exists
    if data is None:
        return response.text('Map does not exist', status=400)

    response_data = {'map': json.loads(data['map'])}

    # Return the stored data
    return response.json(response_data)


# Listen for POST requests on the /dashboard endpoint and store the given data in a database.
@app.post('/dashboard')
async def save_dashboard(request):
    # Ensure that the data is valid
    if not validate(request.json, dashboard_schema):
        return response.text('Data is invalid', status=400)

    table = get_table_for_request(request, 'dashboards')

    # If the user has a saved dashboard already, find it in the database
    saved_dashboard = table.find_one(username=request.json['username'])

    data = {
        'dashboard': json.dumps(request.json['dashboard']),
        'username': request.json['username']
    }

    # Attempt to store the new data
    try:
        if saved_dashboard is None:
            table.insert(data)
        else:
            table.update(data, ['username'])
    except Exception as e:
        return response.text('Error saving data in database', status=500)

    # Notify the user that the dashboard was stored successfully
    return response.text('OK')


# Listen for GET requests on the /dashboard endpoint and return the specified dashboard
@app.get('/dashboard')
async def get_dashboard(request):

    # Get the username that identifies the target data
    try:
        username = request.args.get('username')
    except:
        return response.text('Missing username', status=400)

    table = get_table_for_request(request, 'dashboards')

    # Search the database for the correct data
    data = table.find_one(username=username)

    # Ensure that the data exists
    if data is None:
        return response.text('Dashboard does not exist', status=400)

    response_data = {'dashboard': json.loads(data['dashboard'])}

    # Return the stored data
    return response.json(response_data)


# Listen for POST requests on the /data endpoint and store the given data in a database.
@app.post('/data')
async def save_data(request):
    # Ensure that a POST body is supplied
    if request.json is None:
        log.warning('Server received invalid request to save data')
        return response.text('Invalid request', status=400)

    table = get_table_for_request(request, 'data')

    # Attempt to store the new data
    try:
        data = {
            'data': json.dumps(request.json['data']),
            'time': request.json['data'][0]['start'],
            'username': request.json['username']
        }
        table.insert(data)

    except Exception as e:
        log.error('Error saving data in database: {0}'.format(str(e)))
        return response.text('Error saving data in database', status=500)

    log.info('Successfully saved data for username {0}'.format(request.json['username']))

    # Notify the user of success
    return response.text('OK')


# Listen for GET requests on the /data endpoint and return the specified data
@app.get('/data')
async def get_data(request):

    # Get the date and ID that identifies the target data
    try:
        test = request.args.get('test')
        username = request.args.get('username')
    except:
        return response.text('Missing test ID or username', status=400)

    table = get_table_for_request(request, 'data')

    # Search the database for the correct data
    data = table.find_one(username=username, time=test)

    # Ensure that the data exists
    if data is None:
        log.warning('Data requested does not exist')
        return response.text('Data does not exist', status=400)

    # Retrieve the target channel, if the user has specified it.
    target_channel = request.args.get('channel', False)

    # Load the data from the saved JSON.
    data = json.loads(data['data'])

    # If there is a target channel...
    if target_channel:
        # Search the data for the correct channel and return data for that channel only, as well as
        # the config.
        for line in data:
            if 'channelname' in line and line['channelname'] == target_channel:
                response_data = {'data': line, 'config': data[1]}
                break
        else:
            # If it doesn't exist, return an error.
            return response.text('Channel not found', status=400)

    else:
        # If there's no target channel, return the whole test data.
        response_data = {
            'data': data,
        }

    log.info('Successfully retrieved data for test {0} with username {1}'.format(test, username))

    return response.json(response_data)


@app.post('/users')
async def add_user(request):

    # Ensure that a POST body is supplied
    if request.json is None:
        return response.text('Invalid request', status=400)

    # Ensure that the data is valid
    if not validate(request.json, new_user_schema):
        return response.text('Data is invalid', status=400)

    username = request.json['username']
    password = request.json['password']
    database = request.json.get('database', default_database_path)

    try:
        auth.store_user(username, password, database)
        return response.text('OK')
    except auth.AuthException as e:
        return response.text(str(e), status=400)


# Handle connections from Pie Clients using a Websocket
@app.websocket('/')
async def handler(request, websocket):
    while True:
        client_packet_json = await websocket.recv()
        log.info('Pie client request recieved: {}'.format(client_packet_json))

        try:
            # Convert JSON string to dictionary
            client_packet = json.loads(client_packet_json)
        except TypeError:
            # We tried de-json'ing something not JSON.
            # Just start loop again (wait for next packet).
            continue
        if client_packet['request'] == 1 or client_packet['request'] == '1':
            # Pie client wants data, we give it data.

            # Request data from Liara server
            request_json = json.dumps({'request': '1'})
            sock.sendto(bytes(request_json, 'UTF-8'), webserver_sock)

            log.info('Waiting for data from Liara server')

            # Wait for server response
            data_compressed, address = sock.recvfrom(1024)

            log.info('Returning data to client...')

            # Format JSON response for Pie clients
            data = json.loads(data_compressed.decode('utf_8'))

            data_formatted = json.dumps({'response': data})

            # Send response to client
            await websocket.send(data_formatted)


def format_date(date):
    return date.strftime('%Y-%m-%d')


# Given a request object and table name, return the correct database table
def get_table_for_request(request, table_name):
    # Allow the user to specify their own database filename.
    # Both GET and POST requests are supported.
    try:
        database = request.json.get('database', default_database_path)
    except AttributeError:
        database = request.args.get('database', default_database_path)

    db = dataset.connect(database)
    return db[table_name]


if __name__ == '__main__':
    # Create command-line argument parser
    parser = argparse.ArgumentParser()

    # Get the IP address and port of the Liara server
    parser.add_argument('--liara_ip', nargs='?', default='0.0.0.0')
    parser.add_argument('--liara_port', nargs='?', type=int, default=7777)

    # Parse command-line arguments for global use
    args = parser.parse_args()
    webserver_sock = (args.liara_ip, args.liara_port)
    app.run(host='0.0.0.0', port=8666)

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
