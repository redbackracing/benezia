def validate(data, schema):
    # The edge case for recursion occurs when data is a value and schema is an atomic type. Return
    # True if the data is of correct type and false otherwise.
    if not isinstance(schema, dict) and not isinstance(schema, list):
        if isinstance(data, bool):
            if schema == bool:
                return True
            else:
                return False

        # 0 is a special case as it could be either an integer or a float.
        if data == 0:
            return isinstance(data, int) or isinstance(data, float)

        return isinstance(data, schema)

    # Otherwise, we need to validate a collection.

    # For lists, call validate on each item.
    if isinstance(schema, list):
        if not isinstance(data, list):
            return False

        # If the list schema is only one item long, it is assumed
        # to be of variable length, where the supplied schema applies to all values.
        if len(schema) == 1:
            return all(validate(item, schema[0]) for item in data)

        # If not, it is restricted to the length of the schema and validators are paired.
        if len(schema) != len(data):
            return False

        return all(
            validate(item, validator) for item, validator in zip(data, schema))

    # For dictionaries, validate each key individually.
    if isinstance(schema, dict):

        # Check that correct keys are provided (i.e. no extra or missing keys)
        if set(data.keys()) != set(schema.keys()):
            return False

        # Validate each value according to the corresponding schema
        return all(validate(value, schema[key]) for key, value in data.items())

    # If the end of this function is reached, something is really wrong.
    return False


# Schema for validating channel data requests
data_schema = {
    'configuration': {
        'configFile': {
            'inputs': [{
                'channelName':
                str,
                'channelNumber':
                int,
                'samplingRate':
                int,
                'readOrder':
                int,
                'valueRange': [float, float],
                'signalRange': [float, float],
                'conversion': [{
                    'x': float,
                    'y': float
                }, {
                    'x': float,
                    'y': float
                }],
            }],
            'outputs': {}
        }
    },
    'channel': {
        'channelnum': int,
        'channelname': str,
        'value': float,
        'time': float,
        'diag': bool,
        'warn': bool
    },
    'database': str,
    'username': str,
}

# Schema for validating new users
new_user_schema = {'username': str, 'password': str, 'database': str}

# Schema for validating dashboards
dashboard_schema = {
    'dashboard': {
        'charts': [{
            'top': int,
            'left': int,
            'height': int,
            'width': int,
            'class': str,
            'dataset': int,
            'channel': int
        }]
    },
    'username': str,
    'database': str
}

map_schema = {
    'map': [{'x': float, 'y': float}],
    'name': str,
    'database': str
}

data_info_schema = {
    'start': str
}

channel_info_schema = {
    'channelname': str,
    'channelnum': int,
    'diag': bool,
    'time': float,
    'value': float,
    'warn': bool
}
