#!/usr/bin/py3.5
# TODO: fix ^^ for alternate python install locations

import snappy

some_data = "hello my name is Mike fgdfgggggggggggggggggggggggg"
print("size of uncompressed data is {}".format(len(some_data)))
compressed = snappy.compress(bytes(some_data, 'utf-8'))
print("size of compressed data is {}".format(len(compressed)))
print("content of compressed data is --- {}".format(compressed))
uncompressed = snappy.uncompress(compressed)
print(uncompressed)