#!/usr/bin/py3.5
# TODO: ^^ fix later

import socket

UDP_IP = "54.206.75.71"
UDP_PORT = 12000
message = "{\"request\": 1}"

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

while True:
    sock.sendto(bytes(message, 'UTF-8'), (UDP_IP, UDP_PORT))  # send request to webserver
    data, addr = sock.recvfrom() # buffer size is 1024 bytes
    print("received message:", data)