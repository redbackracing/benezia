#!/usr/bin/py3.5
#TODO: ^^ check

import asyncio
import websockets
import json

json_req_dict = {"request": 1}
json_req_msg = json.dumps(json_req_dict)


async def get_data():
    async with websockets.connect('ws://localhost:8666') as ws:
        while True:
            await ws.send(json_req_msg)
            print("waiting for data from PieServer")
            data = await ws.recv()
            print(data)


asyncio.get_event_loop().run_until_complete(get_data())
