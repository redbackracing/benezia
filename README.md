# Benezia

To run the server, navigate to the `benezia` directory and run `python
server.py`. A help message will be displayed with required arguments.

To install requirements, run `pip install -r requirements.txt`.

To run tests, navigate to the `tests` directory and run `pytest`.